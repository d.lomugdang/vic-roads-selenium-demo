﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VicRoadsDemo.Interfaces;
using VicRoadsDemo.Models;
using VicRoadsDemo.Models.NSW;

namespace VicRoadsDemo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Automate()
        {
#if DEBUG
            //RenewRegistration renewRegistration = new RenewRegistration(new VicRoadPageModel(new ChromeDriver()));
            RenewRegistration renewRegistration = new RenewRegistration(new NewSouthWalesPageModel(new ChromeDriver()));
#endif

#if !DEBUG
            RenewRegistration renewRegistration = new RenewRegistration(new VicRoadPageModel(new PhantomJSDriver()));
#endif

            renewRegistration.RunAutomate();

            return View("Index");
        }
    }
}