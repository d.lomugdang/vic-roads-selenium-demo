﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VicRoadsDemo.Interfaces
{
    public interface IPageRunner
    {
        void RunSteps(IWebDriver driver);
    }

    public interface IRegisterPage
    {
        LinkedList<IRegisterStep> RegisterSteps();
        IWebDriver PageDriver { get; }
    }
}
