﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VicRoadsDemo.Models.NSW
{
    public class NewSouthWalesPageModel : RegistrationBase
    {
        public NewSouthWalesPageModel(IWebDriver driver)
            : base("NewSouthWales", "https://my.service.nsw.gov.au/MyServiceNSW/index#/rms/trans/renewRegistration/login", driver)
        {
            PlateNumber = "CA96NU"; //CRY84Z - no CTP (green slip)
        }

        public string PlateNumber
        {
            get; set;
        }

        private string DontHaveBillNumber
        {
            get
            {
                return "cant-find-billing-number";
            }
        }

        private string PlateNumberInputField
        {
            get
            {
                return "formly_formly_ng_repeat_input-with-button_plateNumber_0";
            }
        }

        private string SearchPlateNoButton
        {
            get
            {
                return "//input[@type='button' and @value='Search' and @ng-click='to.click()']";
            }
        }

        private string StepOneNextButton
        {
            get
            {
                return "//button[@ng-disabled='isRightDisabled()']";
            }
        }

        private string StepTwoNextButton
        {
            get
            {
                return "//button[@ng-disabled='isRightDisabled()']";
            }
        }

        private string TermsAndConditions
        {
            get
            {
                return "formly_formly_ng_repeat_checkbox-label-with-action_termsAndConditions_18";
            }
        }

        private string StepThreeNextButton
        {
            get
            {
                return "//button[@ng-disabled='isRightDisabled()']";
            }
        }

        private string StepFourCardNumber
        {
            get
            {
                return "card_number";
            }
        }

        private string StepFourCardName
        {
            get
            {
                return "card_name";
            }
        }


        private string StepFourMonthExpiry
        {
            get
            {
                return "expiry_month";
            }
        }

        private string StepFourYearExpiry
        {
            get
            {
                return "expiry_year";
            }
        }

        private string StepFourCVV
        {
            get
            {
                return "card_cvv";
            }
        }

        private string StepFourNextButton
        {
            get
            {
                return "//button[@ng-model='button']";
            }
        }



        public override LinkedList<IRegisterStep> RegisterSteps()
        {
            LinkedList<IRegisterStep> steps = new LinkedList<IRegisterStep>();

            RegisterStep mainStep = new RegisterStep(this.Url);

            StepWebElement elementOne = new StepWebElement(this.DontHaveBillNumber);
            elementOne.WorkOnElement = () =>
            {
                WebDriverWait wait = new WebDriverWait(PageDriver, TimeSpan.FromSeconds(30.00));

                wait.Until(driver1 => ((RemoteWebDriver)PageDriver).ExecuteScript("return document.readyState").Equals("complete"));

                var elem = PageDriver.FindElement(By.ClassName(elementOne.ElementName));
                elem.Click();

                return true;
            };
            mainStep.ElementInputs.Add(elementOne);

            StepWebElement stepOneElemOne = new StepWebElement(this.PlateNumberInputField);
            stepOneElemOne.WorkOnElement = () =>
            {
                WebDriverWait wait = new WebDriverWait(PageDriver, TimeSpan.FromSeconds(15.00));

                wait.Until(driver => ((RemoteWebDriver)PageDriver).ExecuteScript("return document.readyState").Equals("complete"));

                var elem = PageDriver.FindElement(By.Id(stepOneElemOne.ElementName));
                elem.SendKeys(this.PlateNumber);

                return true;
            };
            mainStep.ElementInputs.Add(stepOneElemOne);

            StepWebElement stepOneElemTwo = new StepWebElement(this.SearchPlateNoButton);
            stepOneElemTwo.WorkOnElement = () =>
            {
                var elem = PageDriver.FindElement(By.XPath(stepOneElemTwo.ElementName));
                Actions action = new Actions(PageDriver);

                action.MoveToElement(elem).Perform();

                action.MoveToElement(elem).Click().Perform();

                return true;
            };
            mainStep.ElementInputs.Add(stepOneElemTwo);

            StepWebElement stepOneElemThree = new StepWebElement(this.StepOneNextButton);
            stepOneElemThree.WorkOnElement = () =>
            {

                WebDriverWait wait = new WebDriverWait(PageDriver, TimeSpan.FromSeconds(15.00));

                wait.Until(ExpectedConditions.ElementExists(By.XPath($"//div[@vehicle-plate-number='{this.PlateNumber}']")));

                var elem = PageDriver.FindElement(By.XPath(stepOneElemThree.ElementName));

                elem.Click();

                return true;
            };
            mainStep.ElementInputs.Add(stepOneElemThree);

            StepWebElement stepTwoElemOne = new StepWebElement(this.StepTwoNextButton);
            stepTwoElemOne.WorkOnElement = () =>
            {

                WebDriverWait wait = new WebDriverWait(PageDriver, TimeSpan.FromSeconds(15.00));

                wait.Until(ExpectedConditions.ElementExists(By.XPath("//div[contains(@class,'fa-dot-circle-o')]")));

                var elem = PageDriver.FindElement(By.XPath(stepTwoElemOne.ElementName));

                elem.Click();

                return true;
            };
            mainStep.ElementInputs.Add(stepTwoElemOne);

            StepWebElement stepThreeElementOne = new StepWebElement(this.TermsAndConditions);
            stepThreeElementOne.WorkOnElement = () =>
            {

                WebDriverWait wait = new WebDriverWait(PageDriver, TimeSpan.FromSeconds(15.00));

                var elem = wait.Until(ExpectedConditions.ElementExists(By.Id(stepThreeElementOne.ElementName)));                

                elem.Click();

                return true;
            };
            mainStep.ElementInputs.Add(stepThreeElementOne);

            StepWebElement stepThreeElementTwo = new StepWebElement(this.StepThreeNextButton);
            stepThreeElementTwo.WorkOnElement = () =>
            {

                WebDriverWait wait = new WebDriverWait(PageDriver, TimeSpan.FromSeconds(15.00));

                var elem = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(stepThreeElementTwo.ElementName)));

                elem.Click();

                return true;
            };
            mainStep.ElementInputs.Add(stepThreeElementTwo);

            StepWebElement stepFourElementOne = new StepWebElement(this.StepFourCardNumber);
            stepFourElementOne.WorkOnElement = () =>
            {

                WebDriverWait wait = new WebDriverWait(PageDriver, TimeSpan.FromSeconds(15.00));

                var elem = wait.Until(ExpectedConditions.ElementExists(By.Id(stepFourElementOne.ElementName)));

                elem.SendKeys("4321432143214321");

                return true;
            };
            mainStep.ElementInputs.Add(stepFourElementOne);

            StepWebElement stepFourElementTwo = new StepWebElement(this.StepFourCardName);
            stepFourElementTwo.WorkOnElement = () =>
            {

                WebDriverWait wait = new WebDriverWait(PageDriver, TimeSpan.FromSeconds(15.00));

                var elem = wait.Until(ExpectedConditions.ElementExists(By.Id(stepFourElementTwo.ElementName)));

                elem.SendKeys("User Name");

                return true;
            };
            mainStep.ElementInputs.Add(stepFourElementTwo);

            StepWebElement stepFourElementThree = new StepWebElement(this.StepFourMonthExpiry);
            stepFourElementThree.WorkOnElement = () =>
            {

                WebDriverWait wait = new WebDriverWait(PageDriver, TimeSpan.FromSeconds(15.00));

                var elem = wait.Until(ExpectedConditions.ElementExists(By.Name(stepFourElementThree.ElementName)));

                elem.SendKeys("09");

                return true;
            };
            mainStep.ElementInputs.Add(stepFourElementThree);

            StepWebElement stepFourElementFour = new StepWebElement(this.StepFourYearExpiry);
            stepFourElementFour.WorkOnElement = () =>
            {
                WebDriverWait wait = new WebDriverWait(PageDriver, TimeSpan.FromSeconds(15.00));

                var elem = wait.Until(ExpectedConditions.ElementExists(By.Name(stepFourElementFour.ElementName)));

                elem.SendKeys("19");

                return true;
            };
            mainStep.ElementInputs.Add(stepFourElementFour);

            StepWebElement stepFourElementFive = new StepWebElement(this.StepFourCVV);
            stepFourElementFive.WorkOnElement = () =>
            {
                WebDriverWait wait = new WebDriverWait(PageDriver, TimeSpan.FromSeconds(15.00));

                var elem = wait.Until(ExpectedConditions.ElementExists(By.Id(stepFourElementFive.ElementName)));

                elem.SendKeys("018");

                return true;
            };
            mainStep.ElementInputs.Add(stepFourElementFive);

            StepWebElement stepFourElementSix = new StepWebElement(this.StepFourNextButton);
            stepFourElementSix.WorkOnElement = () =>
            {
                WebDriverWait wait = new WebDriverWait(PageDriver, TimeSpan.FromSeconds(15.00));

                var elem = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(stepFourElementSix.ElementName)));

                elem.Click();

                return true;
            };
            mainStep.ElementInputs.Add(stepFourElementSix);


            steps.AddLast(mainStep);

            return steps;
        }
    }
}
