﻿namespace VicRoadsDemo.Models
{
    public abstract class RegisterPageModel
    {
        public string Name { get; set; }
        public string Url { get; set; }

        public RegisterPageModel(string name, string url)
        {
            this.Name = name;
            this.Url = url;
        }
    }
}