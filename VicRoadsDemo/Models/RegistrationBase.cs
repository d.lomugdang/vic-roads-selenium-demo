﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VicRoadsDemo.Interfaces;

namespace VicRoadsDemo.Models
{
    public abstract class RegistrationBase : IRegisterPage
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public IWebDriver PageDriver { get; private set; }

        public RegistrationBase(string name, string url, IWebDriver driver)
        {
            this.Name = name;
            this.Url = url;
            this.PageDriver = driver;
        }

        public abstract LinkedList<IRegisterStep> RegisterSteps();
    }
}
