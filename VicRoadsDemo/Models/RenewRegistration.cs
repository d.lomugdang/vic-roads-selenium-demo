﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using VicRoadsDemo.Interfaces;
using OpenQA.Selenium.Support.UI;

namespace VicRoadsDemo.Models
{
    public class RenewRegistration : IPageRunner
    {
        private IRegisterPage _model;

        public RenewRegistration(IRegisterPage model)
        {
            _model = model;
        }

        internal void RunAutomate()
        {
            RunSteps(_model.PageDriver);
        }        

        public void RunSteps(IWebDriver driver)
        {
            if ((object)driver.Manage().Window != null)
                driver.Manage().Window.Maximize();

            foreach (RegisterStep step in _model.RegisterSteps())
            {
                driver.Navigate().GoToUrl(step.Url);

                foreach (var elemInput in step.ElementInputs.Where(item => !item.IsForSubmit))
                {
                    while (!elemInput.WorkDone)
                        elemInput.DoWork();
                }
            }

            driver.Close();
            driver.Dispose();
        }
    }

    public class StepWebElement
    {
        public StepWebElement(string elementName)
        {
            ElementName = elementName;
            IsForSubmit = false;
        }
        internal void DoWork()
        {
            WorkDone = WorkOnElement.Invoke();
        }

        public string ElementName { get; private set; }
        public bool IsForSubmit { get; set; }
        public Func<bool> WorkOnElement;
        public bool WorkDone = false;
    }

    public class RegisterStep : IRegisterStep
    {
        public string Url { get; private set; }

        public List<StepWebElement> ElementInputs = new List<StepWebElement>();

        public RegisterStep(string url)
        {
            Url = url;
        }
    }
}
