﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VicRoadsDemo.Models
{
    public class VicRoadPageModel : RegistrationBase
    {
        public VicRoadPageModel(IWebDriver driver)
            : base("VicRoads", "https://www.vicroads.vic.gov.au/registration/renew-update-or-cancel/pay-your-registration/renew-your-registration", driver)
        {
        }

        //[PageElement(Step.One,InputType.Select)]
        public string VehicleType
        {
            get
            {
                return "ph_pagebody_0_phthreecolumnmaincontent_0_panel_VehicleType_DDList";
            }
        }

        //[PageElement(Step.One,InputType.Text)]
        public string RegistrationNumber
        {
            get
            {
                return "ph_pagebody_0_phthreecolumnmaincontent_0_panel_RegistrationNumber_RegistrationNumber_CtrlHolderDivHidden";
            }
        }

        //[PageElement(Step.One,InputType.Submit)]
        public string PageOneSubmit
        {
            get
            {
                return "ph_pagebody_0_phthreecolumnmaincontent_0_panel_btnNext";
            }
        }

        //[PageElement(Step.Two,InputType.Submit)]
        public string PageTwoSubmit
        {
            get
            {
                return "ph_pagebody_0_phthreecolumnmaincontent_0_panel_btnNext";
            }
        }

        //[PageElement(Step.Three,InputType.Select)]
        public string PaymentMethod
        {
            get
            {
                return "ph_pagebody_0_phthreecolumnmaincontent_0_panel_paymentPanel_MakePaymentNewNotStored_PaymentMethodDD_DDList";
            }
        }

        //[PageElement(Step.Three,InputType.Text)]
        public string NameOnCard
        {
            get
            {
                return "ph_pagebody_0_phthreecolumnmaincontent_0_panel_paymentPanel_MakePaymentNewNotStored_txtCardholderName_TxtName";
            }
        }

        //[PageElement(Step.Three,InputType.Text)]
        public string CardNumber
        {
            get
            {
                return "ph_pagebody_0_phthreecolumnmaincontent_0_panel_paymentPanel_MakePaymentNewNotStored_txtCreditCardNumber_PaymentSingleLine";
            }
        }

        //[PageElement(Step.Three,InputType.Select)]
        public string Expiry_Month
        {
            get
            {
                return "ddMonth";
            }
        }

        //[PageElement(Step.Three,InputType.Select)]
        public string Expiry_Year
        {
            get
            {
                return "ddYear";
            }
        }

        //[PageElement(Step.Three,InputType.Text)]
        public string CVV
        {
            get
            {
                return "ph_pagebody_0_phthreecolumnmaincontent_0_panel_paymentPanel_MakePaymentNewNotStored_txtCVV_PaymentSingleLine";
            }
        }

        //[PageElement(Step.Three,InputType.Submit)]
        public string PageThreeSubmit
        {
            get
            {
                return "ph_pagebody_0_phthreecolumnmaincontent_0_panel_btnNext";
            }
        }

        public override LinkedList<IRegisterStep> RegisterSteps()
        {
            LinkedList<IRegisterStep> steps = new LinkedList<IRegisterStep>();

            RegisterStep mainStep = new RegisterStep(this.Url);

            StepWebElement elementOne = new StepWebElement(this.VehicleType);
            elementOne.WorkOnElement = () =>
            {
                WebDriverWait wait = new WebDriverWait(PageDriver, TimeSpan.FromSeconds(30.00));

                wait.Until(driver1 => ((RemoteWebDriver)PageDriver).ExecuteScript("return document.readyState").Equals("complete"));

                var elem = PageDriver.FindElement(By.Id(elementOne.ElementName));
                elem.SendKeys("CAR / TRUCK");

                return true;
            };
            mainStep.ElementInputs.Add(elementOne);

            StepWebElement elementTwo = new StepWebElement(this.RegistrationNumber);
            elementTwo.WorkOnElement = () =>
            {
                var elem = PageDriver.FindElement(By.Id(elementTwo.ElementName));
                elem.SendKeys("ZWS124");

                return true;
            };
            mainStep.ElementInputs.Add(elementTwo);

            StepWebElement elementSubmit = new StepWebElement(this.PageOneSubmit);
            elementSubmit.WorkOnElement = () =>
            {
                IWebElement elem;

                while (!PageDriver.FindElement(By.ClassName("progress-bar-title")).Text.Contains("Step 2 of 4 : Confirm details"))
                {
                    elem = PageDriver.FindElement(By.Id(elementSubmit.ElementName));
                    elem.Click();

                    PageDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
                }

                elem = null;

                return true;
            };
            mainStep.ElementInputs.Add(elementSubmit);

            StepWebElement elementConfirmDetails = new StepWebElement(this.PageTwoSubmit);
            elementConfirmDetails.WorkOnElement = () =>
            {
                IWebElement elem;

                while (!PageDriver.FindElement(By.ClassName("progress-bar-title")).Text.Contains("Step 3 of 4 : Make payment"))
                {
                    elem = PageDriver.FindElement(By.Id(elementConfirmDetails.ElementName));
                    elem.Click();
                }

                elem = null;

                return true;
            };
            mainStep.ElementInputs.Add(elementConfirmDetails);

            StepWebElement elementChooseCard = new StepWebElement(this.PaymentMethod);
            elementChooseCard.WorkOnElement = () =>
            {
                WebDriverWait _wait = new WebDriverWait(PageDriver, new TimeSpan(0, 0, 30))
                {
                    PollingInterval = TimeSpan.FromMilliseconds(250)
                };

                _wait.Until(d => d.FindElement(By.ClassName("progress-bar-title")).Text.Contains("Step 3 of 4 : Make payment"));

                var elem = PageDriver.FindElement(By.Id(elementChooseCard.ElementName));
                elem.SendKeys("Visa");

                return true;
            };
            mainStep.ElementInputs.Add(elementChooseCard);

            StepWebElement elemCardName = new StepWebElement(this.NameOnCard);
            elemCardName.WorkOnElement = () =>
            {
                WebDriverWait _wait = new WebDriverWait(PageDriver, new TimeSpan(0, 1, 0))
                {
                    PollingInterval = TimeSpan.FromMilliseconds(250)
                };

                _wait.Until(d => d.FindElements(By.ClassName("loading-mask")).Count() == 0);

                var elem = PageDriver.FindElement(By.Id(elemCardName.ElementName));
                elem.SendKeys("John Doe");

                return true;
            };
            mainStep.ElementInputs.Add(elemCardName);

            StepWebElement elemCardNumber = new StepWebElement(this.CardNumber);
            elemCardNumber.WorkOnElement = () =>
            {
                var elem = PageDriver.FindElement(By.Id(elemCardNumber.ElementName));
                elem.SendKeys("4321432143214321");

                return true;
            };
            mainStep.ElementInputs.Add(elemCardNumber);

            StepWebElement elemExpMonth = new StepWebElement(this.Expiry_Month);
            elemExpMonth.WorkOnElement = () =>
            {
                var elem = PageDriver.FindElement(By.Id(elemExpMonth.ElementName));
                elem.SendKeys("10");

                return true;
            };
            mainStep.ElementInputs.Add(elemExpMonth);

            StepWebElement elemExpYear = new StepWebElement(this.Expiry_Year);
            elemExpYear.WorkOnElement = () =>
            {
                var elem = PageDriver.FindElement(By.Id(elemExpYear.ElementName));
                elem.SendKeys("19");

                return true;
            };
            mainStep.ElementInputs.Add(elemExpYear);

            StepWebElement elemCVV = new StepWebElement(this.CVV);
            elemCVV.WorkOnElement = () =>
            {
                var elem = PageDriver.FindElement(By.Id(elemCVV.ElementName));
                elem.SendKeys("111");

                return true;
            };
            mainStep.ElementInputs.Add(elemCVV);

            ////TODO: Finalize process first
            //StepWebElement elemPayNow = new StepWebElement(this.PageThreeSubmit);
            //elemPayNow.WorkOnElement = () =>
            //{
            //    var elem = PageDriver.FindElement(By.Id(elemPayNow.ElementName));
            //    elem.Click();

            //    return true;
            //};
            //mainStep.ElementInputs.Add(elemPayNow);

            steps.AddLast(mainStep);

            return steps;
        }
    }
}
